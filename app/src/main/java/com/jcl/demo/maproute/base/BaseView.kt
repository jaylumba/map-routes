package com.jcl.demo.maproute.base

/**
 * Created by jaylumba on 05/16/2018.
 */

interface BaseView {

    fun isNetworkAvailable(): Boolean

    fun setLoadingIndicator(active: Boolean)

    fun showToastSuccess(message: String)

    fun showToastError(message: String)

    fun showToastWarning(message: String)

    fun showToastInfo(message: String)

    fun showToastNormal(message: String)

    fun setToolbarTitle(title: String)

    fun updateToolbarTitle(title: String)

    fun isValidEmail(email: String): Boolean

    fun getStringResource(stringId: Int): String
}

package com.exist.phr.data.api

import com.jcl.demo.maproute.data.model.other.GetDirectionResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by jaylumba on 05/16/2018.
 */

interface ApiInterface {
    @GET("/maps/api/directions/json")
    fun getDirection(@Query("origin") origin: String, @Query("destination") destination: String,
                     @Query("key") key: String):Single<GetDirectionResponse>
}

package com.jcl.demo.maproute.data.api

import com.exist.phr.data.api.ApiInterface
import com.jcl.demo.maproute.data.model.other.GetDirectionResponse
import com.jcl.demo.maproute.util.scheduler.AppSchedulerProvider
import io.reactivex.Single

/**
 * Created by jaylumba on 05/16/2018.
 */
class MapService (private val apiInterface: ApiInterface, val schedulerProvider: AppSchedulerProvider) {

    fun getDirection(origin: String, destination: String, key: String) : Single<GetDirectionResponse> {
        return apiInterface.getDirection(origin, destination, key)
                .observeOn(schedulerProvider.ui())
                .subscribeOn(schedulerProvider.io())
    }
}
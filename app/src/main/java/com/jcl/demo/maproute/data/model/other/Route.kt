package com.jcl.demo.maproute.data.model.other

import com.jcl.demo.maproute.data.model.google.Step

data class Route(val startName: String = "",
                 val endName: String = "",
                 val startLat: Double?,
                 val startLng: Double?,
                 val endLat: Double?,
                 val endLng: Double?,
                 val overviewPolyline: String = "",
                 var steps: List<Step>? = null)
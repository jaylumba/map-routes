package com.jcl.demo.maproute.di

import android.content.Context
import com.exist.phr.data.api.ApiInterface
import com.jcl.demo.maproute.data.api.MapService
import com.google.gson.GsonBuilder
import com.jakewharton.picasso.OkHttp3Downloader
import com.jcl.demo.maproute.BuildConfig
import com.jcl.demo.maproute.application.MyApplication
import com.jcl.demo.maproute.data.prefs.SecuredPrefs
import com.jcl.demo.maproute.util.scheduler.AppSchedulerProvider
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by jaylumba on 05/16/2018.
 */
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: MyApplication): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(application: MyApplication): OkHttpClient {
        /** initialize ok http client  */
        var cacheDir = application.externalCacheDir
        if (cacheDir == null) {
            cacheDir = application.cacheDir
        }
        val cache = Cache(cacheDir!!, (10 * 1024 * 1024).toLong())

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .cache(cache)
                .connectTimeout(CONNECTION_TIME_OUT.toLong(), TimeUnit.SECONDS)
                .readTimeout(READ_TIME_OUT.toLong(), TimeUnit.SECONDS)
                .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(application: MyApplication): Retrofit {
        val gson = GsonBuilder().setLenient().create()

        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.HOST_NAME_)
                .client(provideOkHttpClient(application))
                .build()
    }

    @Provides
    @Singleton
    fun provideApiInterface(application: MyApplication): ApiInterface {
        return provideRetrofit(application).create<ApiInterface>(ApiInterface::class.java)
    }

    @Provides
    @Singleton
    fun providePrefs(application: MyApplication): SecuredPrefs {
        return SecuredPrefs(application)
    }

    @Provides
    @Singleton
    fun providePicasso(application: MyApplication): Picasso {
        return Picasso.Builder(application)
                .executor(Executors.newSingleThreadExecutor())
                .downloader(OkHttp3Downloader(provideOkHttpClient(application))).build()

    }

    @Provides
    @Singleton
    fun provideAppSchedulerProvider(): AppSchedulerProvider {
        return AppSchedulerProvider()
    }

    @Provides
    @Singleton
    fun provideMapService(apiInterface: ApiInterface, appSchedulerProvider: AppSchedulerProvider): MapService {
        return MapService(apiInterface, appSchedulerProvider)
    }


    companion object {
        private val CONNECTION_TIME_OUT = 60
        private val READ_TIME_OUT = 60
    }
}

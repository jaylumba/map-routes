package com.jcl.demo.maproute.di

import com.jcl.demo.maproute.ui.map.MapActivity
import com.jcl.demo.maproute.ui.map.di.MapModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by jaylumba on 11/18/2017.
 */

@Module
abstract class BuildersModule{
    @ContributesAndroidInjector(modules = arrayOf(MapModule::class))
    internal abstract fun bindMapActivity(): MapActivity
}

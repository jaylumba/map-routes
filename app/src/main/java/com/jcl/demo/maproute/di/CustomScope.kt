package com.jcl.demo.maproute.di

import javax.inject.Scope

/**
 * Created by jaylumba on 11/18/2017.
 */

@MustBeDocumented
@Scope
@Retention
annotation class CustomScope
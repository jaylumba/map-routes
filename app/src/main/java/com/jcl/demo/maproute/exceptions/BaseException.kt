package com.jcl.demo.maproute.exceptions

open class BaseException(cause: Throwable) : RuntimeException(cause)

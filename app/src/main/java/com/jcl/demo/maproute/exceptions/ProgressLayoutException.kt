package com.jcl.demo.maproute.exceptions

class ProgressLayoutException(ex: Exception) : BaseException(ex)

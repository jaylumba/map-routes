package com.jcl.demo.maproute.ui.map

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.snackbar.Snackbar
import com.google.maps.android.PolyUtil
import com.jcl.demo.maproute.R
import com.jcl.demo.maproute.base.BaseActivity
import com.jcl.demo.maproute.data.model.google.Step
import com.jcl.demo.maproute.data.model.other.Route
import com.jcl.demo.maproute.ui.map.mvp.MapContract
import com.jcl.demo.maproute.ui.map.mvp.MapPresenter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_map.*
import kotlinx.android.synthetic.main.include_app_bar.*
import javax.inject.Inject


class MapActivity : BaseActivity(), MapContract.View, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    @Inject
    lateinit var presenter: MapPresenter

    private lateinit var mMap: GoogleMap
    private var mLocationRequest: LocationRequest? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLastLocation: Location? = null
    private var mRouteMarkerList = ArrayList<Marker>()
    private lateinit var mRoutePolyline: Polyline

    var snackbarInstruction: Snackbar? = null
    var destination: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(com.jcl.demo.maproute.R.layout.activity_map)
        presenter.onLoad()
    }

    override fun onPause() {
        super.onPause()
        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
        }
    }

    override fun setUpToolbar() {
        setSupportActionBar(toolbar)
        setToolbarTitle("Maps")

        btnGetDirection.setOnClickListener {
            destination = etDestination.text.toString()
            if (mLastLocation != null) {
                val origin = mLastLocation?.latitude.toString() + "," + mLastLocation?.longitude.toString()
                presenter.getDirection(origin, destination)
            }
        }
    }

    override fun setUpMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(com.jcl.demo.maproute.R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun displayDirection(route: Route) {
        setMarkersAndRoute(route)
    }

    override fun displayGetDirectionError(error: String) {
        clearMarkersAndRoute()
        showToastError(error)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient()
                mMap.isMyLocationEnabled = true
            } else {
                //Request Location Permission
                checkLocationPermission()
            }
        } else {
            buildGoogleApiClient()
            mMap.isMyLocationEnabled = true
        }
    }

    override fun onConnected(bundle: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest?.interval = 10000
        mLocationRequest?.fastestInterval = 10000
        mLocationRequest?.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if (checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
        }
    }

    override fun onConnectionSuspended(i: Int) {
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
    }

    override fun onLocationChanged(location: Location) {
        //Check if lastlocation = new location. If yes, don't continue
        if (mLastLocation != null && mLastLocation?.latitude == location.latitude && mLastLocation?.longitude == location.longitude)
            return

        mLastLocation = location

        if (destination.isEmpty()) {
            val center = CameraUpdateFactory.newLatLng(LatLng(location.latitude, location.longitude))
            val zoom = CameraUpdateFactory.zoomTo(11f)
            mMap.moveCamera(center)
            mMap.animateCamera(zoom)
            return
        }

        val origin = location.latitude.toString() + "," + location.longitude.toString()
        presenter.getDirection(origin, destination)
    }


    @Synchronized
    private fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient?.connect()
    }

    val MY_PERMISSIONS_REQUEST_LOCATION = 99
    private fun checkLocationPermission() {
        if (checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK") { _, _ ->
                            //Prompt the user once explanation has been shown
                            ActivityCompat.requestPermissions(this@MapActivity,
                                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                    MY_PERMISSIONS_REQUEST_LOCATION)
                        }
                        .create()
                        .show()


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                        MY_PERMISSIONS_REQUEST_LOCATION)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (checkSelfPermission(this,
                                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient()
                        }
                        mMap.isMyLocationEnabled = true
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show()
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }

    private fun setMarkersAndRoute(route: Route) {

        clearMarkersAndRoute()

        val startLatLng = LatLng(route.startLat!!, route.startLng!!)
        val startMarkerOptions: MarkerOptions = MarkerOptions().position(startLatLng).title(route.startName).icon(BitmapDescriptorFactory.fromBitmap(MapsFactory.drawMarker(this, "S")))
        val endLatLng = LatLng(route.endLat!!, route.endLng!!)
        val endMarkerOptions: MarkerOptions = MarkerOptions().position(endLatLng).title(route.endName).icon(BitmapDescriptorFactory.fromBitmap(MapsFactory.drawMarker(this, "E")))
        val startMarker = mMap.addMarker(startMarkerOptions)
        val endMarker = mMap.addMarker(endMarkerOptions)
        mRouteMarkerList.add(startMarker)
        mRouteMarkerList.add(endMarker)

        val polylineOptions = MapsFactory.drawRoute(this)
        val pointsList = PolyUtil.decode(route.overviewPolyline)
        for (point in pointsList) {
            polylineOptions.add(point)
        }

        mRoutePolyline = mMap.addPolyline(polylineOptions)
        mMap.animateCamera(MapsFactory.autoZoomLevel(mRouteMarkerList))

        displayInstructions(route.steps)
    }

    private fun displayInstructions(steps: List<Step>?) {
        if (steps != null) {
            val nextManueverIndex = steps.indexOfFirst { step -> step.maneuver != null }

            if (snackbarInstruction != null && snackbarInstruction!!.isShown) snackbarInstruction!!.dismiss()

            var distance: String = ""

            if (nextManueverIndex == 0) {
                distance = steps[nextManueverIndex].distance.text
            } else if (nextManueverIndex > 0) {
                distance = steps[nextManueverIndex - 1].distance.text
            }

            snackbarInstruction = Snackbar.make(rootLayout, "In " +
                    distance + " " + steps[nextManueverIndex].maneuver,
                    Snackbar.LENGTH_INDEFINITE)

            snackbarInstruction!!.view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGreen))
            val textView = snackbarInstruction!!.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.fontsize_display1))
            snackbarInstruction!!.show()
        }
    }

    fun clearMarkersAndRoute() {
        for (marker in mRouteMarkerList) {
            marker.remove()
        }
        mRouteMarkerList.clear()

        if (::mRoutePolyline.isInitialized) {
            mRoutePolyline.remove()
        }

        if (snackbarInstruction != null && snackbarInstruction!!.isShown) snackbarInstruction!!.dismiss()
    }
}

package com.jcl.demo.maproute.ui.map.di

import com.jcl.demo.maproute.data.api.MapService
import com.jcl.demo.maproute.ui.map.MapActivity
import com.jcl.demo.maproute.ui.map.mvp.MapContract
import com.jcl.demo.maproute.ui.map.mvp.MapPresenter
import com.jcl.demo.maproute.util.scheduler.AppSchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class MapModule {

    @Provides
    internal fun provideView(activity: MapActivity): MapContract.View {
        return activity
    }

    @Provides
    internal fun providePresenter(appSchedulerProvider: AppSchedulerProvider, view: MapContract.View, mapService: MapService): MapPresenter {
        return MapPresenter(appSchedulerProvider, view, mapService)
    }
}

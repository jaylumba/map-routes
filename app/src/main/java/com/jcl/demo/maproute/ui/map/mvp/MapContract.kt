package com.jcl.demo.maproute.ui.map.mvp

import com.jcl.demo.maproute.base.BaseView
import com.jcl.demo.maproute.data.model.other.Route

interface MapContract {
    interface View : BaseView {
        fun setUpToolbar()
        fun setUpMap()
        fun displayDirection(route: Route)
        fun displayGetDirectionError(error: String)
    }

    interface Presenter {
        fun onLoad()
        fun getDirection(origin: String, destination: String)
    }
}

package com.jcl.demo.maproute.ui.map.mvp

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jcl.demo.maproute.BuildConfig
import com.jcl.demo.maproute.base.BasePresenter
import com.jcl.demo.maproute.data.api.MapService
import com.jcl.demo.maproute.data.model.other.Route
import com.jcl.demo.maproute.ui.map.MapActivity
import com.jcl.demo.maproute.util.scheduler.SchedulerProvider
import io.reactivex.Observable
import timber.log.Timber

class MapPresenter(private val schedulerProvider: SchedulerProvider, view: MapContract.View, private val mapService: MapService) : BasePresenter<MapContract.View>(view), MapContract.Presenter {

    private val TAG = MapActivity::class.java.simpleName
    private val requestStateObserver = BehaviorRelay.createDefault(RequestState.IDLE)

    init {
        observeRequestState()
    }

    private fun publishRequestState(requestState: RequestState) {
        addDisposable(Observable.just(requestState)
                .observeOn(schedulerProvider.ui())
                .subscribe(requestStateObserver))
    }

    private fun observeRequestState() {
        addDisposable(requestStateObserver.subscribe({ requestState ->
            when (requestState) {
                RequestState.IDLE -> {
                }
                RequestState.LOADING -> view.setLoadingIndicator(true)
                RequestState.COMPLETE -> view.setLoadingIndicator(false)
                RequestState.ERROR -> view.setLoadingIndicator(false)
                else -> {
                }
            }
        }, { Timber.e(it) }))
    }

    override fun onLoad() {
        view.setUpToolbar()
        view.setUpMap()
    }

    override fun getDirection(origin: String, destination: String) {
        addDisposable(mapService.getDirection(origin, destination, BuildConfig.DIRECTION_KEY)
                .doOnSubscribe { publishRequestState(RequestState.LOADING) }
                .subscribe({ response ->
                    publishRequestState(RequestState.COMPLETE)
                    if (response.status == "OK") {
                        val legs = response.routes[0].legs[0]
                        val route = Route("You are here", destination,
                                legs.startLocation.lat, legs.startLocation.lng,
                                legs.endLocation.lat, legs.endLocation.lng,
                                response.routes[0].overviewPolyline.points,
                                legs.steps)
                        view.displayDirection(route)
                    } else view.displayGetDirectionError(response.status)
                }, { error ->
                    publishRequestState(RequestState.ERROR)
                    view.showToastError(error.localizedMessage)
                }))
    }
}
